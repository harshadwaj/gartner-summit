
##### Learnings from
## Gartner Application Architecture, Development & Integration Summit
##### 24-25 July 2017
---

### Sessions Attended
##### 24 July 2017
<br>
* Tutorial: Designing Application Architectures That FullyEmbrace the Cloud Potential
<br>
* Keynote: Digital Business Transformation: In With the New, Renovate the Old
<br>
* The Role of APIs in the Digital Platform — Enabling Bimodal Integration Patterns

---

* How to Address the Complexities of the Mobile AD Technologies Landscape
<br>
* Magic Quadrant: Enterprise Integration Platform as a Service
<br>
* TechInsights: Choosing a Cloud Platform for Your Microservices Architecture

---

* Magic Quadrant: Full Life Cycle API Management
<br>
* Real-World Automation with Business Rules and Machine Learning
<br>
* Case Study: DevOps and Organisational Transformation from the Trenches

---

* Boomi: Case Study: Teachers Mutual Bank Innovation Roadmap – Technologyvs People
<br>
* Solace: Digital Data River
<br>
* Trends in Cloud Platforms and Event-Driven Application Strategies for Leaders, Planners and Practitioners of IT
<br>
* Guest Keynote: ADAPT — The KeyIngredients Needed to Remain Relevant in an Ever Changing and Disrupted World

---

### Sessions Attended
##### 25 July 2017
<br>
* Roundtable: Server-Side JavaScript Using Node.js: Are You Ready?
<br>
* Blockchain for Architects: Where Blockchain and Smart Contracts Fit in IT Applications
<br>
* How Telstra Leveraged Agile, A Modern Cloud Platform And Innovation To Delight Customers & Drive Business Results

---

<br>
* Data Integration - One-on-One with Gartner Consultant
<br>
* TechInsights: Using Event-Driven Architecture for Microservices, APIs and Integration
<br>
* Innovating With Agile — Taking It to the Next Level

---

<br>
* Guest Closing Keynote: Whythe Future is Much Better Than You Think
<br>
* Closing Remarks
---

### Areas
<br>
* DevOps
* Agile
* Data Analytics
* Application/Integration Architecture
* Healthcare

---

## Learnings from Sessions
<br>
* Shift from Project to Product Culture
* Buy vs. Build
* Bi-Modal Operation
* Negotiate with Cerner for 'Headless Millennium'
---

### Learnings from DevOps Roundtable Session
<br>
* Explained our DevOps setup
* Found that situation-to-maturity ratio is very high
* We are on the right track. Just need more investment
* Next Stage - DevOps to be natural conclusion to the Agile cycle
* Suggested to present in DevOps seminars and meetups
---

### Learnings from Agile One-on-One
<br>
* Agile initiative must be a Meta project
* Best time is the Greenfield time
* A chance not to be missed to have the high impact
* If done properly, at the right time, can be a game changer

---

#### Learnings from Analytics One-on-One
* My Questions
  * How to fail safe?
  * How to be lean and agile in our approach?
  * Tech stack for Health data analytics?
* Takeaways
  * Solving too many things together - MDM, Analytics
  * Need to slow down
  * Take time to research - questions, experts
  * Think strategic

---

## Other Takeaways
<br>
* Availability of Gartner Consultants
* Unification of tools and practices across eHealth
* Tactical Projects should fund background strategic projects
  * Ex. Agile Initiatives
  * DevOps Culture
  * Testability of the products
  * Building Headless capabilities



















